package ch.ethz.matsim.students_fs18.zenhaeusern;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleTypeImpl;

import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVDispatcherConfig;
import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.analysis.simulation.ModeShareListenerModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;
import ch.ethz.matsim.projects.astra.analysis.AnalysisModule;
import ch.ethz.matsim.projects.astra.av.ASTRAAVModule;
import ch.ethz.matsim.projects.astra.av.ASTRAQSimModule;
import ch.ethz.matsim.projects.astra.av.pricing.AVPriceCalculationConfigGroup;
import ch.ethz.matsim.projects.astra.av.pricing.AVPriceCalculationMode;
import ch.ethz.matsim.projects.astra.av.pricing.AVPriceCalculationModule;
import ch.ethz.matsim.projects.astra.av.waiting_time.AVWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.av.waiting_time.AVWaitingTimeCalculatorModule;
import ch.ethz.matsim.projects.astra.av.waiting_time.AVWaitingTimeCalculatorType;
import ch.ethz.matsim.projects.astra.av.waiting_time.kernel.KernelWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.av.waiting_time.zonal.ZonalWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.config.ASTRAConfigGroup;
import ch.ethz.matsim.projects.astra.mode_choice.ASTRAModeChoiceModule;
import ch.ethz.matsim.projects.astra.run.CommandLineConfigurator;
import ch.ethz.matsim.projects.astra.scoring.ASTRAScoringModule;
import ch.ethz.matsim.projects.astra.traffic.ASTRATrafficModule;

public class RunScenario {
	static public void main(String[] args) {
		CommandLineConfigurator cmd = new CommandLineConfigurator(args);

		ASTRAConfigGroup astraConfig = new ASTRAConfigGroup();
		AVPriceCalculationConfigGroup pricingConfig = new AVPriceCalculationConfigGroup();
		AVWaitingTimeCalculatorConfigGroup waitingTimeConfig = new AVWaitingTimeCalculatorConfigGroup();

		Config config = ConfigUtils.loadConfig(cmd.getArguments().get(0), astraConfig, new DvrpConfigGroup(),
				pricingConfig, new AVWaitingTimeCalculatorConfigGroup(), new KernelWaitingTimeCalculatorConfigGroup(),
				new ZonalWaitingTimeCalculatorConfigGroup(), new AVConfigGroup());
		cmd.apply(config);

		double flowEfficiencyFactor = Double.parseDouble(cmd.getArguments().get(1));
		long numberOfVehicles = Long.parseLong(cmd.getArguments().get(2));
		String shapeFile = cmd.getArguments().size() > 3 ? cmd.getArguments().get(3) : null;

		astraConfig.setBaseline(true);
		pricingConfig.setMode(AVPriceCalculationMode.FIXED);
		waitingTimeConfig.setCalculator(AVWaitingTimeCalculatorType.FIXED);

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(AVRoute.class, new AVRouteFactory());
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		Controler controler = new Controler(scenario);

		controler.addOverridingModule(new DvrpTravelTimeModule());
		controler.addOverridingModule(new AVModule());
		controler.addOverridingModule(new BaselineModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new ZurichModule());
		controler.addOverridingModule(new ASTRATrafficModule());
		controler.addOverridingModule(new ASTRAScoringModule());
		controler.addOverridingModule(new ASTRAModeChoiceModule());
		controler.addOverridingModule(new ASTRAAVModule());
		controler.addOverridingModule(new AnalysisModule());
		controler.addOverridingModule(new ASTRAQSimModule());
		controler.addOverridingModule(new AVWaitingTimeCalculatorModule());
		controler.addOverridingModule(new AVPriceCalculationModule());
		controler.addOverridingModule(new ModeShareListenerModule());

		// Configure service

		AVConfig avConfig = new AVConfig();
		AVOperatorConfig operatorConfig = avConfig.createOperatorConfig("sav");

		AVDispatcherConfig dispatcherConfig = operatorConfig.createDispatcherConfig("SingleHeuristic");

		if (shapeFile != null) {
			dispatcherConfig.addParam("areaShapefile", shapeFile);
		}

		AVGeneratorConfig generatorConfig = operatorConfig.createGeneratorConfig("ASTRA");
		generatorConfig.setNumberOfVehicles(numberOfVehicles);

		// Configure flow efficiency of shared AVs

		VehicleType avType = new VehicleTypeImpl(Id.create("sav", VehicleType.class));
		avType.setFlowEfficiencyFactor(flowEfficiencyFactor);

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				bind(VehicleType.class).annotatedWith(Names.named(AVModule.AV_MODE)).toInstance(avType);
				bind(AVConfig.class).toInstance(avConfig);
			}
		});

		controler.run();
	}
}
