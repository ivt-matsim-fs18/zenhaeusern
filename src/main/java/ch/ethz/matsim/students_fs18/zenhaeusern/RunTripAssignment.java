package ch.ethz.matsim.students_fs18.zenhaeusern;

import java.io.File;
import java.util.Arrays;
import java.util.Random;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.population.io.PopulationWriter;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.pt.PtConstants;

import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.projects.astra.av.area.AVServiceArea;
import ch.ethz.matsim.projects.astra.av.area.FullServiceArea;
import ch.ethz.matsim.projects.astra.av.area.SHPServiceArea;
import ch.ethz.matsim.projects.astra.av.tools.FeasibleTripFinder;
import ch.ethz.matsim.projects.astra.av.tools.TripAssignment;
import ch.ethz.matsim.projects.astra.mode_choice.ASTRAChainAlternatives;
import ch.ethz.matsim.projects.astra.utils.LongPlanFilter;

public class RunTripAssignment {
	static public void main(String[] args) {
		String inputPopulationPath = args[0];
		String inputNetworkPath = args[1];
		String inputShapefilePath = args[2];
		String outputPopulationPath = args[3];

		String mode = args[4];
		double probability = Double.parseDouble(args[5]);

		Scenario scenario = ScenarioUtils.createScenario(ConfigUtils.createConfig());
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		new MatsimNetworkReader(scenario.getNetwork()).readFile(inputNetworkPath);
		new PopulationReader(scenario).readFile(inputPopulationPath);

		new LongPlanFilter(10, new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE))
				.run(scenario.getPopulation());

		StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);
		MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();

		ASTRAChainAlternatives chainAlternatives = new ASTRAChainAlternatives(stageActivityTypes, mainModeIdentifier);

		AVServiceArea serviceArea = new FullServiceArea();

		if (!inputShapefilePath.equals("full")) {
			serviceArea = SHPServiceArea.load(scenario.getNetwork(), new File(inputShapefilePath));
		}

		FeasibleTripFinder tripFinder = new FeasibleTripFinder(scenario.getNetwork(), chainAlternatives,
				stageActivityTypes, mainModeIdentifier, serviceArea, Arrays.asList("walk", "pt", "av"),
				Arrays.asList("car", "bike"), "av", Arrays.asList("car", "pt"), false);

		TripAssignment tripAssignment = new TripAssignment(new Random(0), tripFinder, stageActivityTypes, "av", false);

		switch (mode) {
		case "person":
			tripAssignment.assignByPerson(scenario.getPopulation(), probability);
			break;
		case "trip":
			tripAssignment.assignByTrip(scenario.getPopulation(), probability);
			break;
		default:
			throw new IllegalStateException("Unknown mode. Use 'person' or 'trip'.");
		}

		new PopulationWriter(scenario.getPopulation()).write(outputPopulationPath);
	}
}
